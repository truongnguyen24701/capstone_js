import React, { Component } from 'react'
import { phoneArr } from './DataPhone'
import ItemPhone from './ItemPhone'
import TableGioHang from './TableGioHang'

export default class Ex_Phone extends Component {
    state = {
        phoneArr: phoneArr,
        gioHang: JSON.parse(localStorage.getItem("gioHang")) || []
    }

    // cập nhập giỏ hàng thêm sản phẩm
    renderListPhone = () => {
        return this.state.phoneArr.map((item) => {
            return <ItemPhone
                handleShowItem={this.handleShowItem}
                handleAddToCart={this.handleAddToCart}
                phoneData={item}
                key={item.id}
            />
        })
    }


    // cập nhập giỏ hàng 
    handleAddToCart = (sp) => {
        const index = this.state.gioHang.findIndex((item) => {
            return item.id == sp.id
        })
        let cloneGioHang = [...this.state.gioHang]

        if (index == -1) {
            let newSp = {
                ...sp, soLuong: 1, total: new Intl.NumberFormat('en-US', {
                    style: 'currency',
                    currency: 'VND',
                    minimumFractionDigits: 0
                }).format(sp.price)
            }
            cloneGioHang.push(newSp)
        } else {
            cloneGioHang[index].soLuong++
            console.log(cloneGioHang[index]);
            cloneGioHang[index].total = new Intl.NumberFormat('en-US', {
                style: 'currency',
                currency: 'VND',
                minimumFractionDigits: 0
            }).format((cloneGioHang[index].soLuong) * Number(sp.price))
        }
        localStorage.setItem("gioHang", JSON.stringify(cloneGioHang))
        this.setState({
            gioHang: cloneGioHang
        })
    };

    // cập nhập số lượng sp
    handleUpdate = (number, id) => {
        const data = this.state.gioHang.find((item) => {
            return item.id == id
        })
        if (data) {
            if (data.soLuong >= 1) {
                if (number == -1 && data.soLuong == 1) {
                    alert("Vui lòng click chuột vào nút xoá!! xin cảm ơn")
                    return 0
                }
                data.soLuong += number
                data.total = new Intl.NumberFormat('en-US', {
                    style: 'currency',
                    currency: 'VND',
                    minimumFractionDigits: 0
                }).format(data.soLuong * data.price)

                const newGioHang = this.state.gioHang

                newGioHang.map(item => {
                    if (item.id == data.id) {
                        item = data
                    }
                })
                localStorage.setItem("gioHang", JSON.stringify(newGioHang))
                this.setState({
                    gioHang: newGioHang
                })
            }
        }
    }


    // xoá sản phẩm
    handleRemove = (idPhone) => {
        let index = this.state.gioHang.findIndex((item) => {
            return (item.id = idPhone)
        })
        if (index == -1) {
            let cloneGioHang = [...this.state.gioHang]
            cloneGioHang.splice(index, 1)
            localStorage.setItem("gioHang", JSON.stringify(cloneGioHang))
            this.setState({
                gioHang: cloneGioHang
            })
        }
    }

    // đổi type điên thoại
    handleChangePhone = (type) => {
        if (String(type).includes("Điện thoại")) {
            this.setState({
                phoneArr: phoneArr
            })
        }
        if (String(type).includes("iphone")) {
            const dataIphone = phoneArr.filter(value => {
                return value.type == "iphone"
            })
            this.setState({
                phoneArr: dataIphone
            })
        }
        if (String(type).includes("samsung")) {
            const dataSamSung = phoneArr.filter(value => {
                return value.type == "samsung"
            })
            this.setState({
                phoneArr: dataSamSung
            })
        }
    }


    // thanh toán reset về rỗng
    handleThanhToan = () => {
        this.setState({
            gioHang: [],
        })
        localStorage.setItem("gioHang", JSON.stringify([]))
    }

    render() {
        return (
            <div>
                <div className='container py-5'>
                    {this.state.gioHang.length > 0 && (
                        <TableGioHang
                            handleThanhToan={this.handleThanhToan}
                            handleRemove={this.handleRemove}
                            handleUpdate={this.handleUpdate}
                            gioHang={this.state.gioHang} />
                    )}
                    <select onChange={(data) => {
                        this.handleChangePhone(data.target.value)
                    }} className='btn btn-primary my-3'>
                        <option value={"Điện thoại"}>Điện thoại</option>
                        <option value={"iphone"}>iphone</option>
                        <option value={"samsung"}>samsung</option>
                    </select>
                    <div className='row'>
                        {this.renderListPhone()}
                    </div>
                </div>
            </div>
        )
    }
}
