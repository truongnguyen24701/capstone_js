import React, { Component } from 'react'

export default class ItemPhone extends Component {
    render() {
        let { name, price, img, chip, ram, cameraSau, cameraTruoc, pin } = this.props.phoneData
        return (
            <div className='col-4 text-left mb-5'>
                <div className="card" style={{ width: '22rem' }}>
                    <img className="card-img-top" src={img} alt="" />
                    <div className="card-body">
                        <a href="#">{name}</a> <br />
                        <h5 className='card-title'>{new Intl.NumberFormat('en-US', {
                            style: 'currency',
                            currency: 'VND',
                            minimumFractionDigits: 0
                        }).format(price)}</h5>
                        <p>- {chip}</p>
                        <p>- {ram}</p>
                        <p>- Camera Sau: {cameraSau}</p>
                        <p>- Camera trước: {cameraTruoc}</p>
                        <p>- {pin}</p>
                        <button
                            onClick={() => {
                                this.props.handleAddToCart(this.props.phoneData)
                            }}
                            className='btn btn-success'>Add To Cart</button>
                        <button
                            onClick={() => {
                                this.props.handleShowItem(this.DetailPhone)
                            }}
                            className='btn btn-primary mx-4'>Xem chi tiết</button>
                    </div>
                </div>

            </div>
        )
    }
}
