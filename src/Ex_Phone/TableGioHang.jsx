import React, { Component } from 'react'

export default class TableGioHang extends Component {
    renderTable = () => {
        return this.props.gioHang.map((item) => {
            return (
                <tr key={item.id}>
                    <td>{item.id}</td>
                    <td>{item.name}</td>
                    <td>
                        <button
                            onClick={() => {
                                this.props.handleUpdate(-1, item.id)
                            }}
                            className='btn btn-primary mx-2'>-</button>
                        {item.soLuong}
                        <button
                            onClick={() => {
                                this.props.handleUpdate(1, item.id)
                            }}
                            className='btn btn-warning mx-2'>+</button>
                    </td>
                    <td>{
                        new Intl.NumberFormat('en-US', {
                            style: 'currency',
                            currency: 'VND',
                            minimumFractionDigits: 0
                        }).format(item.price)}</td>
                    <td>{item.total}</td>
                    <td>
                        <button
                            onClick={() => {
                                this.props.handleRemove()
                            }}
                            className='btn btn-danger'>Xoá</button>
                    </td>
                </tr>
            )
        })
    }
    render() {
        return (
            <div>
                <table className='table'>
                    <thead>
                        <td>Mã sản phẩm</td>
                        <td>Tên sản phẩm</td>
                        <td>Số lượng</td>
                        <td>Giá tiền</td>
                        <td>Tổng tiền</td>
                        <td>Thao tác</td>
                    </thead>
                    <tbody>
                        {this.renderTable()}
                    </tbody>
                </table>
                <button onClick={() => {
                    this.props.handleThanhToan()
                }} className='btn btn-success'>Thanh Toán</button>
            </div>
        )
    }
}
