import React, { Component } from 'react'

export default class DetailPhone extends Component {
    render() {
        let { name,
            manHinh,
            heDieuHanh,
            img,
            chip,
            ram,
            cameraTruoc,
            cameraSau,
            boNhoTrong,
            sim,
            pin } = this.props.DetailPhone
        return (
            <div className='row'>
                <div className='rol-4'>
                    <img src={img} className="w-100" alt="" />
                </div>
                <div className='col-8 text-left'>
                    <table className='table'>
                        <tbody>
                            <tr>
                                <th className='th_sanpham'>Thông tin kỹ thuật</th>
                            </tr>
                            <tr>Màn hình
                                <td>{manHinh}</td>
                            </tr>
                            <tr>Hệ điều hành
                                <td>{heDieuHanh}</td>
                            </tr>
                            <tr>Camera sau
                                <td>{cameraSau}</td>
                            </tr>
                            <tr>Camera trước
                                <td>{cameraTruoc}</td>
                            </tr>
                            <tr>Chip
                                <td>{chip}</td>
                            </tr>
                            <tr>RAM
                                <td>{ram}</td>
                            </tr>
                            <tr>Bộ nhớ trong
                                <td>{boNhoTrong}</td>
                            </tr>
                            <tr>SIM
                                <td>{sim}</td>
                            </tr>
                            <tr>Pin, Sạc
                                <td>{pin}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        )
    }
}
